package testcases;

import static org.testng.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Pages.DreamerPage;
import io.github.bonigarcia.wdm.WebDriverManager;

public class testcase1 {
	private WebDriver driver = null;
	private JavascriptExecutor js = null;
	private final String projectpath = System.getProperty("user.dir");

	@BeforeTest
	public void setUp() {
		WebDriverManager.chromedriver().version("2.36").setup();
		driver = new ChromeDriver();
		js = (JavascriptExecutor) driver;
		driver.manage().window().maximize();
	}

	@Test
	public void testKonfiguratorDream() throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("https://hoeffner.de/konfigurator/dreamer");
		js.executeScript("window.scrollTo(0,500)");

		// radio button random chooses
		Thread.sleep(1000);
		DreamerPage.radioButton_finder(driver).get(0).click();
		Thread.sleep(500);
		DreamerPage.radioButton_finder(driver).get(8).click();
		Thread.sleep(500);
		DreamerPage.radioButton_finder(driver).get(11).click();
		Thread.sleep(800);

		// click to submit button
		DreamerPage.button_NächsterSchritt(driver).click();
		Thread.sleep(1000);
		assertEquals(driver.getCurrentUrl(), "https://www.hoeffner.de/konfigurator/dreamer/tuerart");
	}

	@AfterTest
	public void tearsDown() throws IOException {
		if (driver != null) {
			takeScreenshot(driver,projectpath);
			driver.close();
			driver.quit();
		}
	}

	public static void takeScreenshot(WebDriver driver, String filePath) throws IOException {
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile, new File(filePath + "/Screenshots/screenshot.png"));
	}

}
