package Pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class DreamerPage {
	
public static List<WebElement> radioButton_finder(WebDriver driver) {
		return driver.findElements(By.className("radioButton"));
	}

public static WebElement button_NächsterSchritt(WebDriver driver) {
	return driver.findElement(By.id("submitStepButtonBottomId"));
}

}
